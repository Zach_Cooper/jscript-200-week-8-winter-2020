//describe('Blackjack', () => {
//    describe('calcPoints', () => {
//        const ten = {
//            val: 10,
//            displayVal: '10'
//        };
//        const six = {
//            val: 6,
//            displayVal: '6'
//        };
//        const ace = {
//           val: 1,
//            displayVal: Ace
//        };


//        it('Handles standard numeric cards correctly.', () => {
//            const cardCount = calcPoints([ten, six]);

//            const expected = {
//                total: 16,
//                isSoft: false
//            };

//            expect(cardCount).toEqual(expected);
//        });

//       it('Handles ace correctly.', () => {
//            const cardCount = calcPoints([ten, six, ace]);

//            const expected = {
//                total: 17,
//                isSoft: true
//            };

//            expect(cardCount).toEqual(expected);

//        });
//    });
//});

/// Create unit tests using Jasmine to test:

describe('dealerShouldDraw', () => {
    const ten = {
        val: 10,
        displayVal = '10'
    };

    const six = {
        val: 6,
        displayVal = '6'
    };

    const Ace = {
        val: 11,
        displayVal = 'Ace'
    };

    const nine = {
        val: 9,
        displayVal = '9'
    };

    const two = {
        val: 2,
        displayVal = '2'
    };

    const four = {
        val: 4,
        displayVal = '4'
    };

    const five = {
        val: 5,
        displayVal = '5'
    };

    it('Handles dealer to draw 1 correctly', () => {
        const cardCount = calcPoints([four, nine]);
            const expected = {
                total = 13,
                isSoft = false
        };
        expect(cardCount).toEqual(expected);

        });

    it('Handles dealer to draw 2 correctly', () => {
        const cardCount = calcPoints([ace, four]);
            const expected = {
                total = 15,
                isSoft = true
        };
        expect(cardCount).toEqual(expected);

        });

    
    it('Handles dealer to draw 3 correctly', () => {
        const cardCount = calcPoints([ace, four, nine]);
            const expected = {
                total = 14,
                isSoft = false
        };
        expect(cardCount).toEqual(expected);

        });

    
    it('Handles dealer to draw 4 correctly', () => {
        const cardCount = calcPoints([two, four, five, two]);
            const expected = {
                total = 13,
                isSoft = false
        };
        expect(cardCount).toEqual(expected);

        });
    });


    
