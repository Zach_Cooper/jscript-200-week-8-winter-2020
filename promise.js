
const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
      const random = Math.random();
      if (random > 0.5) {
        resolve(random);
      } else {
        reject();
      }
    }, 1000);
});

promise
  .then(number => {
  console.log( `Sucess, ${number.toFixed(3)}`);
  })
  .catch(e => {
    console.log(`fail`);
  });
