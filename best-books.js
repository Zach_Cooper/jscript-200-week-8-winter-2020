const formEl = document.getElementById('best-books-form');
const yearEl = document.getElementById('year');
const monthEl = document.getElementById('month');
const dateEl = document.getElementById('date');

formEl.addEventListener('submit', function(e) {
  e.preventDefault();

  const year = yearEl.value;
  const month = monthEl.value;
  const date = dateEl.value;

  // Fetch bestselling books for date and add top 5 to page
  const BASE_URL = `https://api.nytimes.com/svc/books/v3/lists/${year}-
  ${month}-${date}/combined-print-and-e-book-nonfiction.json`;

  const url = `${BASE_URL}?api-key=${API_KEY}`;

  fetch(url)
    .then(function(data) {
      return data.json();
    })
    .then(function(responsJson) {
      for (i = 0; i < 5; i++) {
        let books = responsJson.results.books[i];
        document.getElementById('search-header').innerText = `Showing Results for: ${year}-
  ${month}-${date}`;

        const title = books.title;
        const author = books.author;
        const description = books.description;

        let booksCont = document.getElementById('books-container');
        let lineBreak = document.createElement('BR');

        let titleText = document.createTextNode(`Book Title: ${title}`)
        let authorText = document.createTextNode(`Book Author: ${author}`)
        let descriptionText = document.createTextNode(`Description: ${description}`)

        const newDiv = document.createElement('div')
        newDiv.appendChild(titleText);
        newDiv.appendChild(lineBreak);
        newDiv.appendChild(authorText);
        newDiv.appendChild(lineBreak);
        newDiv.appendChild(descriptionText);
        newDiv.appendChild(lineBreak);

        let img = document.createElement('img');
        img.setAttribute('src', books.book_image);
        newDiv.appendChild(img);

        booksContainer.appendChild(newDiv);


      }
    })
});
