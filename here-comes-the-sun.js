let color = 0;

const animateScreen = function() {
    color++;
    document.querySelector('body').style.backgroundColor= `rgb(${color}, ${color}, ${color})`
        if (color < 255) {
            requestAnimationFrame(animateScreen);
        }
}

requestAnimationFrame(animateScreen);