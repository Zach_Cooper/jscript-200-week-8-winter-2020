// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';

const url = `${BASE_URL}?q=sports&api-key=${API_KEY}`;

fetch(url)
.then(response => {
  console.log(response);
  return response.json();
})
.then(function(responseJson) {
  let article = responseJson.response.docs[0];

  const mainHeadline = article.headline.main;
  document.getElementById('article-title').innerText = mainHeadline;

  if(article.multimedia.length > 0) {
    const imgUrl = `https://www.nytimes.com/${article.multimedia[0].url}`;
    document.getElementById('article-img').src =imgUrl;
  }
});